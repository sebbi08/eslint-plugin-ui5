# Use hungarian notation when declaring variables (hungarian-notation)

We strongly recommend to use the Hungarian notation where name prefixes indicate the type for variables and object field names.

## Rule Details

This rule aims to enforce Hungarian notation. Since JavaScript is dynamically typed this rule makes it easier to see the expected type of a variable.

The following prefixes are recommended:

| Sample | Type |
|-|-|
| `sId` | string |
| `oDomRef` | object |
| `$DomRef` | jQuery object |
| `iCount` | int |
| `mParameters` | map / assoc. array |
| `aEntries` | array |
| `dToday` | date |
| `fDecimal` | float |
| `bEnabled` | boolean |
| `rPattern` | RegExp |
| `fnFunction` | function |
| `vVariant` | variant types |

## Options
This rule has an object option:

### onlyDeclarations
"onlyDeclarations": true requires only var, function, and class declarations to match the specified regular expression

Examples of **incorrect** code for this rule:

```js
var users = ["Adam", "Eve"];

var fnLog = function(text) {
    console.log(text);
};
```
Examples of **correct** code for this rule:

```js
var aUsers = ["Adam", "Eve"];
```

Examples of **correct** code for this rule with the `{ "onlyDeclarations": true }` options:

```js
var fnLog = function(text) {
    console.log(text);
};
```

### whiteList

The whiteList option can hold an array of whitelisted variable names.

Normaly this would be **incorect**.

```js
QUnit.test("I should test my formatters", function(assert) {
    assert.ok(true);
});
```

But with the following configuration `{ "whiteList": [ 'assert' ] }`

the following code would be **correct**.

```js
QUnit.test("I should test my formatters", function(assert) {
    assert.ok(true);
});
```


## When Not To Use It

If you don't want to enforce Hungarian notation do not use this rule.
