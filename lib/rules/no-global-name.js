/**
 * @fileoverview Do not load modules/controls synchronously
 * @author Marc Schleeweiß
 */
"use strict";

//------------------------------------------------------------------------------
// Rule Definition
//------------------------------------------------------------------------------

module.exports = {
    meta: {
        docs: {
            description: "Don't use references to global names",
            category: "Best Practices",
            recommended: true
        },
        fixable: null,  // or "code" or "whitespace"
        schema: [
            // fill in your schema
        ]
    },

    create: function (context) {

        const ERROR_MSG_NO_SYNC_MODULE_LOADING = "Unexpected reference to global name";

        const EXCEPTIONS = [
            "sap.ui.define",
            "sap.ui.require",
            "sap.ui.getCore",
            "sap.ui.component",
            "sap.ui.fragment",
            "sap.ui.htmlfragment",
            "sap.ui.jsfragment",
            "sap.ui.jsview",
            "sap.ui.template",
            "sap.ui.view",
            "sap.ui.xmlfragment",
            "sap.ui.xmlview"
        ];

        //----------------------------------------------------------------------
        // Helpers
        //----------------------------------------------------------------------

        // any helper functions should go here or else delete this section

        //----------------------------------------------------------------------
        // Public
        //----------------------------------------------------------------------

        return {
            MemberExpression(node) {
                if (node.object.name === "sap") {
                    const namespaceParts = [];
                    namespaceParts.push(node.object.name);
                    namespaceParts.push(node.property.name);

                    while (node.parent.type === "MemberExpression") {
                        node = node.parent;
                        namespaceParts.push(node.property.name);
                    }

                    const namespace = namespaceParts.join(".");

                    if (EXCEPTIONS.indexOf(namespace) < 0) {
                        context.report(node, ERROR_MSG_NO_SYNC_MODULE_LOADING);
                    }
                }
            }
        };
    }
};
